import { lazy } from 'react'

const DashboardRoutes = [
  // Dashboards
  {
    path: '/dashboard/',
    component: lazy(() => import('../../views/dashboard/analytics'))
  },
  {
    path: '/marketplace/',
    component: lazy(() => import('../../views/marketplace')),
    exact: true
  },
/*   {
    path: '/marketplace/improveHELOC/',
    component: lazy(() => import('../../views/marketplace/improveHELOC')),
    exact: true
  }, */
  {
    path: '/marketplace/pools/improveHelocPool',
    component: lazy(() => import('../../views/marketplace/pools/improveHelocPool')),
    exact: true
  },
  {
    path: '/marketplace/pools/poolLoanLevel',
    component: lazy(() => import('../../views/marketplace/pools/poolLoanLevel')),
    exact: true
  }
]

export default DashboardRoutes
