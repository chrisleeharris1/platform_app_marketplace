// !Do not remove the Layout import
import Layout from '@layouts/VerticalLayout'

const VerticalLayout = props => (
    <Layout       
    //menu={< CustomMenu/>}  
    {...props}>
        {props.children}
    </Layout>
)


export default VerticalLayout
