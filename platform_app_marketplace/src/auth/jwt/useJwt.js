// ** Core JWT Import
import makeJwt from '@src/@core/auth/jwt/makeJwt'

const { jwt } = makeJwt({})

export default jwt
