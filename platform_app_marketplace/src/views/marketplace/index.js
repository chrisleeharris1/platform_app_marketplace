import { Fragment } from 'react'
import { Card, CardTitle, CardBody, CardText, Row, Col, Button } from 'reactstrap'
import { Link } from 'react-router-dom'


const MarketPlace = () => {
  return (
    <Fragment>
      <h5 className='mt-3 mb-2'>MARKETPLACE</h5>
      <Row>
        <Col md='6' lg='4'>
          <Card className='mb-3'>
            <CardBody>
              <CardTitle tag='h4'>Improve HELOC's</CardTitle>
              <CardText>5-30 year Home Equity Lines of Credit.  $15,000 - $150,000 loan size.  Investors can buy Improve HELOC's outright or through our BWIC system.  Loan information is available to anyone, but bids are private. </CardText>
              <Button.Ripple color='primary' tag={Link} to='/marketplace/pools/improveHelocPool' outline>
                Learn more
              </Button.Ripple>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Fragment>
  )
}

export default MarketPlace
