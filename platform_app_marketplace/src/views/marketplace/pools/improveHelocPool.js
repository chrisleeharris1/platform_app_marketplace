import { Fragment } from 'react'
import { Card, CardTitle, CardBody, CardText, Row, Col, Button } from 'reactstrap'
import { Link } from 'react-router-dom'

//second card
import { InputGroup, InputGroupAddon, Input, InputGroupText } from 'reactstrap'

//document card
import AvatarGroup from '@components/avatar-group'
import react from '@src/assets/images/icons/react.svg'
import vuejs from '@src/assets/images/icons/vuejs.svg'
import angular from '@src/assets/images/icons/angular.svg'
import bootstrap from '@src/assets/images/icons/bootstrap.svg'
import avatar1 from '@src/assets/images/portrait/small/avatar-s-5.jpg'
import avatar2 from '@src/assets/images/portrait/small/avatar-s-6.jpg'
import avatar3 from '@src/assets/images/portrait/small/avatar-s-7.jpg'
import { MoreVertical, Edit, Trash } from 'react-feather'
import { Table, Badge, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'




const improveHELOC = () => {
  return (
    <Fragment>

      <Row>
        <Col md='6' lg='12'>
			{/* about card */}
          <Card className='mb-3'>
            <CardBody>
              <CardTitle tag='h4'>Improve Technologies HELOC pool</CardTitle>
              <CardText>This is a pool of HELOC's originated by Improve Technologies, Inc.  You can bid on these loans here and if you are the highest bidder, 
			  you will be notified immediately as soon as the bidding process is over.  All data needed to diligence the loans is included below.  Loan level
			  information about this pool can also be shown by clicking below.</CardText>
			  <Button.Ripple color='primary' tag={Link} to='/marketplace/pools/poolLoanLevel' 
			  outline>
                Loan-level details
              </Button.Ripple>
			  </CardBody>
          </Card>

			{/* purchase card */}
		  <Card className='mb-3'>
            <CardBody>
			<CardTitle tag='h4'>Purchase</CardTitle>
			  <InputGroup className='mb-2'>
        			<InputGroupAddon addonType='prepend'>
          				<InputGroupText>$</InputGroupText>
        			</InputGroupAddon>
        			<Input placeholder='100' />
      			</InputGroup>
            </CardBody>
          </Card>

		  <Card className='card-profile'>
      
	  	{/* pool details card */}
      <CardBody>
        <div className='profile-image-wrapper'>
        </div>
        <h3>Improve HELOC pool details</h3>
        {/* <h6 className='text-muted'>Improve HELOC pool details</h6> */}
        <hr className='mb-2' />
        <div className='d-flex justify-content-between align-items-center'>
			{/* <div className="d-flex flex-fill"> */}
          <div>
            <h6 className='text-muted font-weight-bolder'>UPB</h6>
            <h3 className='mb-0'>$14,384,342.84</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>WAC</h6>
            <h3 className='mb-0'>9.05</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>WA FICO</h6>
            <h3 className='mb-0'>729</h3>
          </div>
		  <div>
            <h6 className='text-muted font-weight-bolder'>WA CLTV</h6>
            <h3 className='mb-0'>62</h3>
          </div>
        </div>
		<p></p>
		<div className='d-flex justify-content-between align-items-center'>
          <div>
            <h6 className='text-muted font-weight-bolder'>WA DTI</h6>
            <h3 className='mb-0'>10.3k</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>Servicing Fee</h6>
            <h3 className='mb-0'>0.25</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>Loan Type</h6>
            <h3 className='mb-0'>Improve HELOC</h3>
          </div>
		  <div>
            <h6 className='text-muted font-weight-bolder'>Geography</h6>
            <h3 className='mb-0'>US Loans</h3>
          </div>
		</div>  
      </CardBody>
    </Card>

		{/* offer due card */}
		<Card className='card-profile'>
      <CardBody>
        <div className='profile-image-wrapper'>
        </div>
        <div className='d-flex justify-content-between align-items-center'>
          <div>
            <h6 className='text-muted font-weight-bolder'>First Offers Due</h6>
            <h3 className='mb-0'>July 15, 2021</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>Deal Closes</h6>
            <h3 className='mb-0'>July 18, 2021</h3>
          </div>
		</div>  
      </CardBody>
    </Card>

	{/* document table card	 */}
	<Table hover responsive>
      <thead>
        <tr>
          <th>Documents</th>
          <th></th>
          <th></th>
          <th></th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            {/* <img className='mr-75' src={angular} alt='angular' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>Loan Doc</span>
          </td>
          <td></td>
          <td>
            
          </td>
          <td>
            
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={react} alt='react' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>Loan Doc</span>
          </td>
          <td></td>
          <td>
            
          </td>
          <td>
            
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={vuejs} alt='vuejs' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>Loan Doc</span>
          </td>
          <td></td>
          <td>
            
          </td>
          <td>
            
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>Loan Doc</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={bootstrap} alt='bootstrap' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>Loan Doc</span>
          </td>
          <td></td>
          <td>
            
          </td>
          <td>
            
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
      </tbody>
    </Table>
	
		  
        </Col>
      </Row>
    </Fragment>
  )
}

export default improveHELOC 
