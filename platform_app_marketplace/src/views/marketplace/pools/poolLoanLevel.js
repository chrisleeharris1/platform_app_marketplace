import { Fragment } from 'react'
import { Card, CardTitle, CardBody, CardText, Row, Col, Button } from 'reactstrap'
import { Link } from 'react-router-dom'

//document card
import { MoreVertical, Edit, Trash } from 'react-feather'
import { Table, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'




const improveHELOC = () => {
  return (
    <Fragment>

      <Row>
        <Col md='6' lg='12'>
			
		
		  <Card className='card-profile'>
      
	  	{/* pool details card */}
      <CardBody>
        <div className='profile-image-wrapper'>
        </div>
        <h3>Improve HELOC pool details</h3>
        {/* <h6 className='text-muted'>Improve HELOC pool details</h6> */}
        <hr className='mb-2' />
        <div className='d-flex justify-content-between align-items-center'>
			{/* <div className="d-flex flex-fill"> */}
          <div>
            <h6 className='text-muted font-weight-bolder'>UPB</h6>
            <h3 className='mb-0'>$14,384,342.84</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>WAC</h6>
            <h3 className='mb-0'>9.05</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>WA FICO</h6>
            <h3 className='mb-0'>729</h3>
          </div>
		  <div>
            <h6 className='text-muted font-weight-bolder'>WA CLTV</h6>
            <h3 className='mb-0'>62</h3>
          </div>
        </div>
		<p></p>
		<div className='d-flex justify-content-between align-items-center'>
          <div>
            <h6 className='text-muted font-weight-bolder'>WA DTI</h6>
            <h3 className='mb-0'>10.3k</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>Servicing Fee</h6>
            <h3 className='mb-0'>0.25</h3>
          </div>
          <div>
            <h6 className='text-muted font-weight-bolder'>Loan Type</h6>
            <h3 className='mb-0'>Improve HELOC</h3>
          </div>
		  <div>
            <h6 className='text-muted font-weight-bolder'>Geography</h6>
            <h3 className='mb-0'>US Loans</h3>
          </div>
		</div>  
      </CardBody>
    </Card>

	{/* specific loan details card	 */}
	
	<Table hover responsive>
      <thead>
        <tr>
          <th>Loan ID</th>
          <th>Lien Position</th>
          <th>FICO</th>
          <th>Property Value</th>
          <th>Property zip</th>
		  <th>Property use</th>
          <th>CLTV</th>
          <th>Current UPB</th>
          <th>Original Term</th>
		  <th>Current Rate</th>
          <th>Payment Status</th>
          <th>Status</th>
		  <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            {/* <img className='mr-75' src={angular} alt='angular' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>23561</span>
          </td>
		  <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={react} alt='react' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>345692</span>
          </td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={vuejs} alt='vuejs' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>1234</span>
          </td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>Loan Doc</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            {/* <img className='mr-75' src={bootstrap} alt='bootstrap' height='20' width='20' /> */}
            <span className='align-middle font-weight-bold'>23543</span>
          </td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
		  <td></td>
          <td></td>
          <td></td>
		  <td></td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit className='mr-50' size={15} /> <span className='align-middle'>View Document</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Trash className='mr-50' size={15} /> <span className='align-middle'>Delete</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
      </tbody>
    </Table>		  
        </Col>
      </Row>
    </Fragment>
  )
}

export default improveHELOC 
